import React from 'react';
import {View, Text, StyleSheet, Image, TouchableOpacity} from 'react-native';
import TreeView from '../components/TreeView';
import {productsWithPaths} from '../utils/functions';

const DrawerContent = props => {
  const {
    selectedVariants,
    handleSelectVariant,
    handleSelectAllVariants,
    handleRemoveVariant,
    deselectVariant,
  } = props;

  const RenderSelectedVariants = () => {
    return Object.keys(selectedVariants).map((model, index) => (
      <View key={index} style={styles.variantGroup}>
        <View style={styles.variantBox}>
          <Text style={styles.variantModel}>{model}</Text>
          <TouchableOpacity onPress={() => handleRemoveVariant(model)}>
            <Text style={styles.removeButton}>-</Text>
          </TouchableOpacity>
        </View>
      </View>
    ));
  };

  return (
    <View style={styles.container}>
      <View style={styles.profileSection}>
        <Image
          source={require('../assets/profile-icon.png')}
          style={styles.profileImage}
        />
        <Text style={styles.profileName}>Adeel Cheema</Text>
        <Text style={styles.profileDetails}>03121212121</Text>
      </View>
      <View style={styles.divider} />
      <Text style={styles.header}>Browse Products</Text>
      <TreeView
        data={productsWithPaths}
        onSelectVariant={handleSelectVariant}
        onSelectAllVariants={handleSelectAllVariants}
        deselectVariant={deselectVariant}
        selectedVariants={selectedVariants}
        path={''}
      />
      <View style={styles.selectedContainer}>
        <Text style={styles.header}>Selected Variants</Text>
        <View style={styles.selectedVariantsRow}>
          <RenderSelectedVariants />
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    paddingLeft: 5,
  },
  profileSection: {
    alignItems: 'center',
    marginBottom: 16,
    marginTop: 16,
  },
  profileImage: {
    width: 80,
    height: 80,
    borderRadius: 40,
    marginBottom: 8,
  },
  profileName: {
    fontSize: 18,
    fontWeight: 'bold',
    color: 'black',
  },
  profileDetails: {
    fontSize: 14,
    color: 'black',
  },
  divider: {
    height: 1,
    backgroundColor: '#ddd',
    marginVertical: 16,
    width: '100%',
  },
  header: {
    fontSize: 18,
    marginBottom: 16,
    color: 'black',
    fontWeight: 'bold',
  },
  selectedContainer: {
    marginTop: 24,
  },
  selectedVariantsRow: {
    flexDirection: 'row',
    flexWrap: 'wrap',
  },
  variantGroup: {
    marginRight: 8,
    marginBottom: 8,
  },
  variantBox: {
    backgroundColor: '#ddd',
    padding: 8,
    borderRadius: 4,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  variantModel: {
    fontWeight: 'bold',
    marginRight: 8,
    color: 'black',
  },
  removeButton: {
    color: 'red',
    fontSize: 16,
  },
});

export default DrawerContent;
