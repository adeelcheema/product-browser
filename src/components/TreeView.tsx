import React from 'react';
import {View} from 'react-native';
import TreeNode from './TreeNode';

const TreeView: React.FC<TreeViewProps> = ({
  data,
  onSelectVariant,
  onSelectAllVariants,
  deselectVariant,
  selectedVariants,
}) => {
  return (
    <View>
      {data.map((item, index) => (
        <TreeNode
          key={index}
          node={item}
          path={item.name}
          onSelectVariant={onSelectVariant}
          onSelectAllVariants={onSelectAllVariants}
          deselectVariant={deselectVariant}
          isChecked={selectedVariants[item.name]}
          selectedVariants={selectedVariants}
        />
      ))}
    </View>
  );
};

export default TreeView;
