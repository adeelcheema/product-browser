import React, {useState, useEffect, useCallback} from 'react';
import {View, Text, StyleSheet, TouchableOpacity} from 'react-native';
import CheckBox from '@react-native-community/checkbox';

const TreeNode: React.FC<{
  node: TreeNodeProps;
  path: string;
  onSelectVariant?: (path: string, variant: string) => void;
  onSelectAllVariants?: (
    path: string,
    selected: boolean,
    isParent?: boolean,
  ) => void;
  deselectVariant?: (path: string) => void;
  isChecked?: boolean;
  selectedVariants: {[model: string]: boolean};
}> = ({
  node,
  path,
  onSelectVariant,
  onSelectAllVariants,
  deselectVariant,
  isChecked = false,
  selectedVariants,
}) => {
  
  const [expanded, setExpanded] = useState(false);
  const [checked, setChecked] = useState(isChecked);

  useEffect(() => {
    setChecked(isChecked);
  }, [isChecked]);

  const handleSelectAll = useCallback(() => {
    const newCheckedState = !checked;
    setChecked(newCheckedState);

    if (onSelectAllVariants) {
      onSelectAllVariants(path, newCheckedState, true);

      if (node.children) {
        node.children.forEach(child => {
          const childPath = `${path} > ${child.name}`;
          if (newCheckedState) {
            onSelectAllVariants(childPath, false, false);
          }
        });
      }
    }
  }, [checked, onSelectAllVariants, node.children, path]);

  const handleChildSelect = useCallback(
    (childPath: string, selected: boolean) => {
      onSelectAllVariants && onSelectAllVariants(childPath, selected, false);

      if (!selected) {
        setChecked(false);
        if (onSelectAllVariants) {
          onSelectAllVariants(path, false, true);
        }
      } else if (node.children) {
        const allChildrenSelected = node.children.every(
          child => selectedVariants[`${path} > ${child.name}`],
        );
        if (allChildrenSelected) {
          setChecked(true);
        }
      }
    },
    [onSelectAllVariants, node.children, path, selectedVariants],
  );

  return (
    <View style={styles.node}>
      <View style={styles.nodeHeader}>
        <CheckBox
          value={checked}
          onValueChange={handleSelectAll}
          tintColors={{true: '#acabdf', false: 'grey'}}
          style={styles.checkbox}
        />
        <TouchableOpacity onPress={() => setExpanded(!expanded)}>
          <Text style={styles.nodeText}>{node.name}</Text>
        </TouchableOpacity>
      </View>
      {expanded && node.children && (
        <View style={styles.children}>
          {node.children.map((child, index) => {
            const childPath = `${path} > ${child.name}`;
            const childIsChecked = checked;
            return (
              <TreeNode
                key={index}
                node={child}
                path={childPath}
                onSelectVariant={onSelectVariant}
                onSelectAllVariants={handleChildSelect}
                deselectVariant={deselectVariant}
                isChecked={childIsChecked}
                selectedVariants={selectedVariants}
              />
            );
          })}
        </View>
      )}
    </View>
  );
};

const styles = StyleSheet.create({
  node: {
    marginVertical: 4,
  },
  nodeHeader: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  nodeText: {
    fontSize: 16,
    color: 'black',
  },
  children: {
    paddingLeft: 16,
  },
  checkboxContainer: {
    padding: 0,
    margin: 0,
  },
  checkbox: {
    marginRight: 8,
    transform: [{scaleX: 0.7}, {scaleY: 0.7}],
  },
});

export default TreeNode;
