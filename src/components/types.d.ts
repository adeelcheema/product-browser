interface TreeNodeProps {
  name: string;
  children?: TreeNodeProps[];
  path: string;
  onSelectVariant?: (path: string, variant: string) => void;
  onSelectAllVariants?: (
    path: string,
    selected: boolean,
    isParent?: boolean,
  ) => void;
  deselectVariant?: (path: string) => void;
  isChecked?: boolean;
}

interface TreeViewProps {
  data: TreeNodeProps[];
  path: string;
  onSelectVariant: (path: string, variant: string) => void;
  onSelectAllVariants: (
    path: string,
    selected: boolean,
    isParent?: boolean,
  ) => void;
  deselectVariant: (path: string) => void;
  selectedVariants: {[model: string]: boolean};
}
