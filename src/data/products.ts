import { Category } from "./types";

export const products: Category[] = [
  {
    name: 'Phones',
    children: [
      {
        name: 'Apple',
        children: [
          {
            name: 'iPhone 6',
            children: [
              {name: '128GB', price: 299, image: require('../assets/iphone-6.png')}, 
              {name: '256GB', price: 349, image: require('../assets/iphone-6.png')}, 
              {name: '512GB', price: 399, image: require('../assets/iphone-6.png')}
            ],
          },
          {
            name: 'iPhone 7',
            children: [
              {name: '128GB', price: 399, image: require('../assets/iphone-7.png')}, 
              {name: '256GB', price: 449, image: require('../assets/iphone-7.png')}, 
              {name: '512GB', price: 499, image: require('../assets/iphone-7.png')}
            ],
          },
        ],
      },
      {
        name: 'Samsung',
        children: [
          {
            name: 'Galaxy S21',
            children: [
              {name: '128GB', price: 799, image: require('../assets/s21.png')}, 
              {name: '256GB', price: 849, image: require('../assets/s21.png')}, 
              {name: '512GB', price: 899, image: require('../assets/s21.png')}
            ],
          },
        ],
      },
    ],
  },
  {
    name: 'Computers',
    children: [
      {
        name: 'Apple',
        children: [
          {
            name: 'MacBook Pro',
            children: [
              {name: '256GB', price: 1299, image: require('../assets/macbook-pro.png')}, 
              {name: '512GB', price: 1499, image: require('../assets/macbook-pro.png')}, 
              {name: '1TB', price: 1799, image: require('../assets/macbook-pro.png')}
            ],
          },
        ],
      },
    ],
  },
  {
    name: 'Watches',
    children: [
      {
        name: 'Apple',
        children: [
          {
            name: 'Apple Watch Series 6',
            children: [
              {name: '40mm', price: 399, image: require('../assets/apple-watch-series-6.png')}, 
              {name: '44mm', price: 429, image: require('../assets/apple-watch-series-6.png')}
            ],
          },
          {
            name: 'Apple Watch SE',
            children: [
              {name: '40mm', price: 279, image: require('../assets/apple-watch-series-se.jpeg')}, 
              {name: '44mm', price: 309, image: require('../assets/apple-watch-series-se.jpeg')}
            ],
          },
        ],
      },
      {
        name: 'Samsung',
        children: [
          {
            name: 'Galaxy Watch 4',
            children: [
              {name: '40mm', price: 249, image: require('../assets/apple-watch-series-6.png')}, 
              {name: '44mm', price: 279, image: require('../assets/apple-watch-series-6.png')}
            ],
          },
        ],
      },
    ],
  },
  {
    name: 'TVs',
    children: [
      {
        name: 'Samsung',
        children: [
          {
            name: 'Samsung QLED',
            children: [
              {name: '55 inch', price: 1099, image: require('../assets/samsung-qled.png')}, 
              {name: '65 inch', price: 1299, image: require('../assets/samsung-qled.png')}, 
              {name: '75 inch', price: 1499, image: require('../assets/samsung-qled.png')}
            ],
          },
        ],
      },
      {
        name: 'Apple',
        children: [
          {
            name: 'Apple TV 4K',
            children: [
              {name: '32GB', price: 179, image: require('../assets/apple-tv-4k.png')}, 
              {name: '64GB', price: 199, image: require('../assets/apple-tv-4k.png')}
            ],
          },
        ],
      },
    ],
  },
];
