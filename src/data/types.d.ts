export interface Variant {
    name: string;
    price: number;
    image: string;
  }
  
  interface Model {
    name: string;
    children?: Variant[];
  }
  
  interface Brand {
    name: string;
    children?: Model[];
  }
  
  interface Category {
    name: string;
    children?: Brand[];
  }
  