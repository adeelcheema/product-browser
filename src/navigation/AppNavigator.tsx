import * as React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createDrawerNavigator} from '@react-navigation/drawer';
import HomeScreen from '../screens/HomeScreen';
import CustomDrawerContent from '../components/DrawerContent';

const Drawer = createDrawerNavigator();

export const AppNavigator = () => {
  //lifted state
  const [selectedVariants, setSelectedVariants] = React.useState({});

  const handleSelectVariant = path => {
    setSelectedVariants(prev => {
      const updatedVariants = {...prev};
      updatedVariants[path] = true;
      return updatedVariants;
    });
  };

  const handleSelectAllVariants = (path, selected) => {
    setSelectedVariants(prev => {
      const updatedVariants = {...prev};
      if (selected) {
        updatedVariants[path] = true;
      } else {
        delete updatedVariants[path];
      }
      return updatedVariants;
    });
  };

  const handleRemoveVariant = model => {
    setSelectedVariants(prev => {
      const updatedVariants = {...prev};
      delete updatedVariants[model];
      return updatedVariants;
    });
    deselectVariant(model);
  };

  const deselectVariant = path => {
    handleSelectAllVariants(path, false);
  };

  return (
    <NavigationContainer>
      <Drawer.Navigator
        initialRouteName="Home"
        drawerContent={props => (
          <CustomDrawerContent
            {...props}
            selectedVariants={selectedVariants}
            handleSelectVariant={handleSelectVariant}
            handleSelectAllVariants={handleSelectAllVariants}
            handleRemoveVariant={handleRemoveVariant}
            deselectVariant={deselectVariant}
          />
        )}>
        <Drawer.Screen name="Home">
          {props => (
            <HomeScreen {...props} selectedVariants={selectedVariants} />
          )}
        </Drawer.Screen>
      </Drawer.Navigator>
    </NavigationContainer>
  );
};
