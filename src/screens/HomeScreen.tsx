import React from 'react';
import {View, Text, StyleSheet, ScrollView, Image} from 'react-native';
import {products} from '../data/products';

const HomeScreen = ({selectedVariants}) => {
  const RenderProducts = () => {
    const allVariants = [];
    const hasSelectedVariants = Object.keys(selectedVariants).length > 0;
    const variantsToDisplay = hasSelectedVariants
      ? allVariants.filter(variant => {
          return Object.keys(selectedVariants).some(selectedPath =>
            variant.fullPath.startsWith(selectedPath),
          );
        })
      : allVariants;

    products.forEach(category => {
      category.children.forEach(brand => {
        brand.children.forEach(model => {
          model.children.forEach(variant => {
            const fullPath = `${category.name} > ${brand.name} > ${model.name} > ${variant.name}`;
            allVariants.push({
              ...variant,
              model: model.name,
              brand: brand.name,
              category: category.name,
              fullPath,
            });
          });
        });
      });
    });

    return (
      <View style={styles.productRow}>
        {variantsToDisplay.map((variant, index) => (
          <View key={index} style={styles.productCard}>
            <Image source={variant.image} style={styles.productImage} />
            <Text style={styles.productName}>{variant.name}</Text>
            <Text style={styles.productDetails}>
              {variant.model} - {variant.name}
            </Text>
            <Text style={styles.productPrice}>${variant.price}</Text>
          </View>
        ))}
      </View>
    );
  };

  return (
    <View style={styles.container}>
      <ScrollView contentContainerStyle={styles.scrollContainer}>
        <Text style={styles.header}>Products</Text>
        <RenderProducts />
      </ScrollView>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#f5f5f5',
    padding: 16,
  },
  scrollContainer: {
    paddingBottom: 16,
  },
  header: {
    fontSize: 18,
    marginBottom: 16,
    color: 'black',
    fontWeight: 'bold',
  },
  productRow: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    justifyContent: 'space-between',
  },
  productCard: {
    width: '48%',
    padding: 16,
    marginBottom: 16,
    backgroundColor: 'white',
    borderRadius: 8,
    shadowColor: '#000',
    shadowOpacity: 0.1,
    shadowRadius: 10,
    elevation: 2,
  },
  productName: {
    fontSize: 16,
    fontWeight: 'bold',
    marginBottom: 8,
  },
  productDetails: {
    fontSize: 14,
    color: 'gray',
    marginBottom: 8,
  },
  productPrice: {
    fontSize: 14,
    color: 'gray',
  },
  productImage: {
    width: '100%',
    height: 100,
    resizeMode: 'contain',
    marginBottom: 8,
  },
});

export default HomeScreen;
