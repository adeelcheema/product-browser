interface SelectedVariants {
    [model: string]: boolean;
  }
  interface Category {
    name: string;
    children?: Category[];
  }

  