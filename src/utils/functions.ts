import {products} from '../data/products';

const mapCategoryToTreeNodeProps = (
  category: Category,
  parentPath: string = '',
): TreeNodeProps => {
  const currentPath = `${parentPath}/${category.name}`;
  return {
    ...category,
    path: currentPath,
    children: category.children?.map(child =>
      mapCategoryToTreeNodeProps(child, currentPath),
    ),
  };
};

export const productsWithPaths: TreeNodeProps[] = products.map(category =>
  mapCategoryToTreeNodeProps(category),
);
